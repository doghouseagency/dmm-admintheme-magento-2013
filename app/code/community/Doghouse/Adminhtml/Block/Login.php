<?php

class Doghouse_Adminhtml_Block_Login extends Mage_Adminhtml_Block_Template {

    public function getStoreName() {
        $storeId = Mage::app()->getStore()->getId();
        return Mage::getStoreConfig('general/store_information/name', $storeId);
    }

    /**
     * @todo, check http_host on *.ubuntu.vm *.dhm.io
     * @return string staging|live|development
     */
    public function getEnvironment() {

        $httphost = Mage::getSingleton('core/app')
            ->getRequest()
            ->getHttpHost();

        $ubuntu_preg    = '/^.*\.ubuntu\.vm$/';
        $stack_preg     = '/^.*\.(stack|perth|melbourne)$/';

        $staging_preg   = '/^.*\.dhm\.io$/';

        //Not really sure how to do this one.. so I'll just leave it out
        //$live_preg      = '/^.*\.com|(au)$/';

        // DEV
        if (preg_match($ubuntu_preg, $httphost)
            || preg_match($stack_preg, $httphost)
        ) {
            return 'Dev';
        }

        // STAGING
        if (preg_match($staging_preg, $httphost)) {
            return 'Staging';
        }

    }

    public function getLoginLabel() {
        $storename      = $this->getStoreName();
        $environment    = $this->getEnvironment();
        $button         = $this->__('Sign in');

        if ($storename !== 'Default' && !empty($storename)) {
            $button = $this->__('Sign in to %s', $storename);
        }

        if ($environment) {
            $button .= sprintf(' (%s)', $environment);
        }

        return $button;
    }

}
