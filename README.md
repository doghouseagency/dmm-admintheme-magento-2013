Doghouse Magento Admin Theme
============================

This is an adminhtml theme and a module that applies Doghouse branding to the admin login page.

Requirements
------------

+ Modman (optional)
+ Tested with Magento 1.8.0.0 and 1.8.1.0
+ Might not work with Magento websites that have a different fallback mechanism (Aoe_DesignFallback or Sonassi_Enterprise)
+ If using template files in a modman module, you must enable "Allow Symlinks" (found under System > Configuration > Advanced > Developer)

Installation
------------

Install using Composer (preferred method) or Modman or clone this repository and copy the files to your Magento website.

### Installation using Composer

Require `"doghouse/admin-theme": "*"` in your composer.json and add this repository to the repositories. As with all Magento modules this module requires `"magento-hackathon/magento-composer-installer":"*"`.

### Installation using Modman

First allow Magento to use template files that are symlinked - see https://github.com/colinmollenhour/modman

cd to your Magento projects root, then:

`modman init`

`modman clone <repo url>`


About
-------

Designed by Benjamin, implemented by Erfan. Open to suggestions.
